unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
  TField = class(TObject)
  private
    function GetAsString:string; virtual; abstract;
    procedure SetAsString(const Value:string); 
              virtual; abstract;
  public
    property AsString:string read GetAsString
                         write SetAsString;
  end;

  TStringField=class(TField)
  private
    FData : string;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;

  TIntegerField = class(TField)
  private
    FData : integer;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;

  TFloatField=class(TField)
  private
    FData : extended;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;

end;
  TDateTimeField = class(TField)
  private
    FData : TDateTime;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;

procedure ShowData(AField:TField);

var
  Form1: TForm1;

implementation

{$R *.dfm}

function TStringField.GetAsString:string;
begin
  Result := FData;
end;

procedure TStringField.SetAsString(const Value:string);
begin
  FData := Value;
end;

function TIntegerField.GetAsString:string;
begin
  Result := IntToStr(FData);
end;

procedure TIntegerField.SetAsString(const Value:string);
begin
  FData := StrToInt(Value);
end;

function TFloatField.GetAsString:string;
begin
  Result := FloatToStrF(FData,ffFixed,7,2);
end;

procedure TFloatField.SetAsString(const Value:string);
begin
  FData := StrToFloat(Value);
end;

function TDateTimeField.GetAsString:string;
begin
  Result := DateTimeToStr(FData);
end;

procedure TDateTimeField.SetAsString(const Value:string);
begin
  FData := StrToDateTime(Value);
end;

procedure ShowData(AField:TField);
begin
   Form1.Lbl1.Caption := AField.AsString;
end;


end.
